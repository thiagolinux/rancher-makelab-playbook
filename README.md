# Ansible Playbook K8s/Rancher MakeLab

## Objetivo: 
Configurar ambiente com cluster K8s usando o RKE1 e instalar o Rancher para gestão WEB do cluster e recursos do k8s

## Pré Requisitos
* No mínimo 3 nós
* Testado em Ubuntu 22.04 LTS
* Acesso SSH nos hosts alvo para execução do playbook
* Definição da URL de acesso ao Rancher (`rancher_url`) e configuração no DNS com registro `A` para qualquer nó do cluster 

## Características
* Ordem de execução das roles ansible: `Instalação do docker`, `Instalação do cluster K8s via RKE` e `Instalação do Rancher no cluster via Helm`
* O primeiro node no arquivo `hosts` será usado para instalar os binários (rke, helm e kubectl) e irá centralizar as tarefas administrativas
* Configurações acima para o usuário `root` no primeiro node.
* Os 3 primeiros nodes serão usados como todas as `roles` do k8s `controlplane, etcd e worker` os demais nós serão usados como `workers`
* Cada ansible role possui variáveis padrões, não sendo inicialmente necessário nenhuma alteração
* O Rancher é instalando via Helm Chart oficial, configurado para apenas `1 réplica` e usando o certificado interno/padrão gerenciado pelo rancher
* O Playbook cria uma nova ssh_key_id `(id_rsa_rke)` no node1 e distribui o .pub para todos os nós do cluster, para ser usado no momento de execução do RKE (Conexão porta TCP/22)

## Excecução

**1 - Preparar o ansible host**

* Instalar o ansible (apt install ansible, etc)
* Instalar dependencias do playbook `ansible-galaxy collection install kubernetes.core`

**2 - Editar o arquivo `hosts`, segue exemplo**

```
[k8s_nodes]
node1 ansible_host=192.168.1.100 
node2 ansible_host=192.168.1.101
node3 ansible_host=192.168.1.102
node4 ansible_host=192.168.1.103

[all:vars]
rancher_url="myrancher.lab"
```

**3 - Executar o playbook `lab.yml`**

```
ansible-playbook lab.yml -i hosts -b
```

O Comando acima executa `todas` as ansible roles na ordem:

* `docker`
* `rke`
* `rancher-helm`

Algumas tasks, como o `RKE Installer`, constuma demorar alguns minutos, devido a inúmeras atividades nessa task

**OBS:** Em alguns laboratórios, ao executar todos as ansible roles de um vez, como acima, dependendo do desempenho do hardware, pode acontecer de algumas tasks serem executadas quando o resultado de uma task anterior ainda ter sido finalizada por completo, como por exemplo: Instalar o rancher no cluster, antes do cluster estar totalmente operacional. Para esses casos, a sugestão é executar um por vez, como descrito abaixo e acompanhar a finalização por completo de todas as task, principalmente na instalação do rke, aguardar todos os Pods estarem operacionais, antes de instalar o rancher.

Para executar ansible roles específicas, em caso de alguma falha e quiser repetir apenas parte do processo, como por exemplo, apenas a instalação do cluster K8s via RKE, execute:

```
# Apenas RKE
ansible-playbook lab.yml -i hosts -b --tags rke
# Apenas Docker
ansible-playbook lab.yml -i hosts -b --tags docker
# Apenas o Rancher
ansible-playbook lab.yml -i hosts -b --tags rancher-helm
```

## Pós instalação

* Acesse a url definida em `rancher_url` e configure a senha administravida para o rancher no primeiro login
* Acesse o node1 como `root` e verifique os nós do cluster `kubectl get nodes -o wide`


## Vagrant

Testado em hosts virtuais usando o Vagrant com Libvirt e Virtualbox. 

Para ambientes sem vagrant, **DESCONSIDERAR**:

* Arquivo: `ansible.cfg` nesse repositório, em conjunto com a linha: `config.ssh.insert_key = false` no Vagrantfile, permite acessar via ssh diretamente pelo ansible, com as sshkeys padrões do vagrant 
* Configurações no hosts como: `ansible_host=127.0.0.1 ansible_port=2222` apenas usado para integração com o Vagrant

**Vagrant com Virtual**

Testado usando a private network (HostOnly), com essa configuração a VM é configurada com 2 (duas) interfaces de rede, que causou alguns problemas no RKE como relatado nessa issue:

* https://github.com/rancher/rancher/issues/22584#issuecomment-698826330

Para contornar esse problema, foi adicionado a variável: `vagrant_virtualbox` do tipo booleana, que quando `true` (default: false) usará o endereço IPv4 da segunda interface de rede (Hardcoded com o nome **eth1**) para os IPs internos dos nós do cluster K8s, com configurações adicionais na rede default do rke (canal).

**OBS:** Em Labs usando "vagrant+libvirt" e o box: "generic/ubuntu2204", é configurado apenas 1 interface, não gerando o problema relatado.

Exemplo, declarando que a variavel como `true`

```
[k8s_nodes]
node1 ansible_host=127.0.0.1 ansible_port=2222 
node2 ansible_host=127.0.0.1 ansible_port=2200
node3 ansible_host=127.0.0.1 ansible_port=2201
node4 ansible_host=127.0.0.1 ansible_port=2202

[all:vars]
rancher_url="myrancher.lab"
vagrant_virtualbox=true
```

Com o valor definido como `false` (default) serão considerados os IPs informados em ansible_host=IP nas configurações do cluster.yml do RKE, para o uso sem vagrant.

Exemplo:
```
[k8s_nodes]
node1 ansible_host=192.168.1.100
node2 ansible_host=192.168.1.101
node3 ansible_host=192.168.1.102
node4 ansible_host=192.168.1.103

[all:vars]
rancher_url="myrancher.lab"
```