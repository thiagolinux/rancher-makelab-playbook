---
- name: Download RKE binary
  get_url:
    url: "https://github.com/rancher/rke/releases/download/{{ rke_release }}/rke_linux-amd64"
    checksum: "sha256:{{ rke_binary_checksum }}"
    dest: /usr/local/bin/rke
    mode: "+rx"
  register: rke_binary

- name: Ensure config directory exists
  file:
    path: "{{ rke_config_dir }}"
    state: directory

- name: Create/update RKE config file
  template:
    src: rke_cluster.yml.j2
    dest: "{{ rke_config_dir }}/cluster.yml"
    mode: u=rw,g=r,o=
    backup: true
  register: rke_cluster_config

- name: Run RKE installer
  shell: 
    cmd: rke up --config cluster.yml  --ignore-docker-version | tee {{ rke_config_dir }}/rke-up-$(date "+%F-%Hh%Mm%Ss").log
  args:
    chdir: "{{ rke_config_dir }}"
  when: rke_force_configure | bool
  register: rke_output
  

- debug: msg="{{ rke_output }}"
  when: rke_output

- name: Ensure $HOME/.kube exists
  file:
    path: "{{ ansible_env.HOME }}/.kube"
    state: directory

- name: Copy kubeconfig to default location
  copy:
    remote_src: yes
    src: "{{ rke_config_dir }}/kube_config_cluster.yml"
    dest: "{{ ansible_env.HOME }}/.kube/config"
    mode: u=rw,g=,o=
  when: rke_force_configure | bool  

- name: Install kubectl
  get_url:
    url: https://storage.googleapis.com/kubernetes-release/release/{{ kubernetes_version.split('-', 1)[0] }}/bin/linux/amd64/kubectl
    dest: /usr/local/bin/kubectl
    force: yes
    mode: "+rx"
  when: rke_force_configure | bool

- name: install pip3
  apt: name=python3-pip state=present 

- name: install pre-requisites
  pip:
    name:
      - pyyaml
      - kubernetes

- name: Get a list of all Nodes
  kubernetes.core.k8s_info:
    kind: Node
  register: node_list
  when: rke_force_configure | bool

# https://stackoverflow.com/a/75477579
- debug:
    var: >-
      node_list.resources 
        | map(attribute='metadata.name')